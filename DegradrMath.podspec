Pod::Spec.new do |s|

s.name         = 'DegradrMath'
s.version      = '0.3.0'
s.license      = { :type => 'MIT', :file => 'LICENSE' }
s.author       = { 'denn nevera' => 'denn.nevera@gmail.com' }
s.homepage     = 'http://degradr.photo'
s.summary      = 'DegradrMath is a basic maths operations for Degradr project.'
s.description  = 'DegradrMath is a basic maths operations for Degradr project....'

s.platform     = :ios, '8.0'
s.source       = { :git => 'https://denn_nevera@bitbucket.org/degrader/degradr-math.git', :tag => '0.3.0'}

s.source_files        = 'DegradrMath/Classes/DPMath.h', 'DegradrMath/Classes/*.{h,m}'
s.public_header_files = 'DegradrMath/Classes/DPMath.h', 'DegradrMath/Classes/*.h'

s.frameworks   = 'UIKit'
s.requires_arc = true

end