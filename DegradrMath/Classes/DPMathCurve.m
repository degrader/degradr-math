//
//  DPSpline.m
//  DegradrMath
//
//  Created by denis svinarchuk on 22.09.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

#import "DPMathCurve.h"
#import <UIKit/UIKit.h>

@interface DP3DCurve()
@end

@implementation DP3DCurve

- (instancetype) initWithXPoints:(NSArray *)xPoints withYPoints:(NSArray *)yPoints withZMatrix:(NSArray *)zMatrix{
    self = [super init];
    if (self) {
        _xPoints = [xPoints mutableCopy];
        _yPoints = [yPoints mutableCopy];
        _zMatrix = [zMatrix mutableCopy];
    }
    return self;
}

- (NSString*) description{
    NSMutableString *s = [@"" mutableCopy];
    
    [s appendString:@"MX=["];
    for (NSNumber *n in self.xPoints) {
        [s appendFormat:@"%4.3f ",[n doubleValue]];
    }
    [s appendString:@"];\n"];

    [s appendString:@"MY=["];
    for (NSNumber *n in self.yPoints) {
        [s appendFormat:@"%4.3f ",[n doubleValue]];
    }
    [s appendString:@"];\n"];

    [s appendString:@"MC="];
    DPMatrix *z = [[DPMatrix alloc] initWithArray:self.zMatrix columns:self.xPoints.count rows:self.yPoints.count];
    [s appendString:[z description]];
    [s appendString:@";"];
    return s;
}

@end


@implementation DPMathCurve

+ (DP3DCurve*) make3DCatmullRomWithCurveControls:(DP3DCurve*)curveControls inXPoints:(NSArray *)xPoints inYPoints:(NSArray *)yPoints{
    DP3DCurve *curve = [[DP3DCurve alloc] init];
    
    curve.xPoints = [xPoints mutableCopy];
    curve.yPoints = [yPoints mutableCopy];
    curve.zMatrix = [NSMutableArray arrayWithCapacity:xPoints.count*yPoints.count];
    
    DPMatrix  *z  = [[DPMatrix alloc] initWithArray:curveControls.zMatrix columns:curveControls.xPoints.count rows:curveControls.yPoints.count];
    
    //
    // y-z
    //
    NSMutableArray *ysplines = [NSMutableArray arrayWithCapacity:yPoints.count];
    for (NSUInteger i=0; i<curveControls.xPoints.count; i++) {
        
        NSArray *Py=[z columnAtIndex:i];
        
        NSMutableArray *points = [NSMutableArray new];
        
        for (NSUInteger yi=0; yi<curveControls.yPoints.count; yi++) {
            float x = [curveControls.yPoints[yi] floatValue];
            float y = [Py[yi] floatValue];
            [points addObject:[NSValue valueWithCGPoint:CGPointMake(x, y)]];
        }
        NSArray *tmp_spline = [self make2DCatmullRomWithControls:points inXPoints:yPoints withValue:YES];
        [ysplines addObjectsFromArray:tmp_spline];
    }

    z = [[DPMatrix alloc] initWithArray:ysplines columns:yPoints.count rows:curveControls.xPoints.count];
    
    for (NSUInteger i=0; i<yPoints.count; i++) {
        NSArray *Py=[z columnAtIndex:i];
        
        NSMutableArray *points = [NSMutableArray new];
        
        for (NSUInteger xi=0; xi<Py.count; xi++) {
            float x = [curveControls.xPoints[xi] floatValue];
            float y = [Py[xi] floatValue];
            [points addObject:[NSValue valueWithCGPoint:CGPointMake(x, y)]];
        }
        NSArray *tmp_spline = [self make2DCatmullRomWithControls:points inXPoints:xPoints withValue:YES];
        [curve.zMatrix addObjectsFromArray:tmp_spline];
    }
    
    z = [[DPMatrix alloc] initWithArray:curve.zMatrix columns:xPoints.count rows:yPoints.count];
    
    return curve;
}

+ (void) find:(NSUInteger[2])k closeControls:(NSArray*)controlPoints Xi:(float)Xi count:(NSUInteger)n{
    NSUInteger k1 = 0;
    NSUInteger k2 = n-1;
    while (k2-k1 > 1){
        NSUInteger k = floor((float)(k2+k1)/2.0);
        NSValue *xk = controlPoints[k];
        CGPoint xkpoint = [xk CGPointValue];
        if (xkpoint.x > Xi){
            k2 = k;
        }
        else{
            k1 = k;
        }
    }
    k[0]=k1;
    k[1]=k2;
}

+ (void) catMullRomSplineGetA:(float*)a b:(float*)b h:(float*)h k:(NSUInteger[2])k controlPoints:(NSArray*)controlPoints count:(NSUInteger)n{

    NSUInteger k1=k[0];
    NSUInteger k2=k[1];
    
    CGPoint P1 = [controlPoints[k1] CGPointValue];
    CGPoint P2 = [controlPoints[k2] CGPointValue];

    *h = P2.x - P1.x;
    
    if (k1 == 0){
        CGPoint P3 = [controlPoints[k2+1] CGPointValue];
        *a = (P2.y - P1.y)/(*h);
        *b = (P3.y - P1.y)/(P3.x - P1.x);
    }
    else if (k2 == n-1){
        CGPoint P0 = [controlPoints[k1-1] CGPointValue];
        *a = (P2.y - P1.y)/(P2.x - P0.x);
        *b = (P2.y - P1.y)/(*h);
    }
    else{
        CGPoint P0 = [controlPoints[k1-1] CGPointValue];
        CGPoint P3 = [controlPoints[k2+1] CGPointValue];
        *a = (P2.y - P0.y)/(P2.x - P0.x);
        *b = (P3.y - P1.y)/(P3.x - P1.x);
    }
}

+ (NSArray*) make2DCatmullRomWithControls:(NSArray *)controlPoints inXPoints:(NSArray *)xPoints{
    return [self make2DCatmullRomWithControls:controlPoints inXPoints:xPoints withValue:NO];
}

+ (NSArray*) make2DCatmullRomWithControls:(NSArray *)controlPoints inXPoints:(NSArray *)xPoints withValue:(BOOL)withValue{
    NSUInteger      n = controlPoints.count;
    
    NSMutableArray *curve;
    
    if (withValue) {
        curve  = [NSMutableArray arrayWithCapacity:xPoints.count];
    }
    else curve  = [NSMutableArray arrayWithCapacity:xPoints.count];
    
    for (NSUInteger i = 0; i<xPoints.count; i++){
        
        float Xi= [xPoints[i] floatValue];
        NSUInteger  k[2];
        
        [self find:k closeControls:controlPoints Xi:Xi count:n];
        
        CGPoint P1 = [controlPoints[k[0]] CGPointValue];
        CGPoint P2 = [controlPoints[k[1]] CGPointValue];
        
        float a,b,h;
        [self catMullRomSplineGetA:&a b:&b h:&h k:k controlPoints:controlPoints count:n];

        float t = (Xi - P1.x)/h;
        float t2 = t*t;
        float t3 = t2*t;
        
        float h00 =  2*t3 - 3*t2 + 1;
        float h10 =    t3 - 2*t2 + t;
        float h01 = -2*t3 + 3*t2;
        float h11 =    t3 - t2;

        CGPoint ci;
        ci.y = h00*P1.y + h10*h*a + h01*P2.y + h11*h*b;
        ci.x = Xi;
        
        if (withValue) {
            [curve addObject:[NSNumber numberWithFloat:ci.y]];
        }
        else{
            [curve addObject:[NSValue valueWithCGPoint:ci]];
        }
    }

    return curve;
}

+ (NSArray*) make2DCatmullRomWithControls:(NSArray *)controlPoints withSize:(NSUInteger)size withInterval:(float)interval{
    return [self make2DCatmullRomWithControls:controlPoints withSize:size withInterval:interval withValue:NO];
}

+ (NSArray*) make2DCatmullRomWithControls:(NSArray *)controlPoints withSize:(NSUInteger)size withInterval:(float)interval withValue:(BOOL)withValue{
    NSMutableArray *xPoints = [NSMutableArray arrayWithCapacity:size];
    float x=0;
    for (NSUInteger i=0; i<size; i++,x+=interval) {
        [xPoints addObject:[NSNumber numberWithFloat:x]];
    }
    return [self make2DCatmullRomWithControls:controlPoints inXPoints:xPoints];
}


+ (NSArray*) make2DUniformWithPoints:(NSArray*)points withSize:(NSUInteger)resulotion withTension:(float)tension{
    return [DPMathCurve make2DSplineWithPoints:points withResolution:resulotion withTension:tension onlyValue:NO];
}

+ (NSArray*) make2DSplineWithPoints:(NSArray*)points withResolution:(NSUInteger)resulotion withTension:(float)tension onlyValue:(BOOL)onlyValue{
    
    NSMutableArray *PP = [NSMutableArray arrayWithCapacity:points.count+2];
    
    [PP addObject:[points firstObject]];
    [PP addObjectsFromArray:points];
    [PP addObject:[points lastObject]];
    
    NSMutableArray *xPoints = [NSMutableArray arrayWithCapacity:points.count];
    
    for (NSUInteger i=0; i<points.count; i++) {
        NSValue *p = points[i];
        [xPoints addObject:@([p CGPointValue].x)];
    }
    
    NSMutableArray *spline = [NSMutableArray arrayWithCapacity:onlyValue?2:resulotion];
    
    if (onlyValue) {
        [spline addObject:[NSMutableArray arrayWithCapacity:resulotion]];
        [spline addObject:[NSMutableArray arrayWithCapacity:resulotion]];
    }
    
    NSArray        *nn     = [DPMathCurve prepareCanonicalSegmentsResolution:resulotion xPoints:xPoints];
    
    for (NSUInteger k=0; k<PP.count-3; k++) {
        NSUInteger nr = [nn[k] unsignedIntegerValue]-1;
        CGPoint P0 = [PP[k] CGPointValue];
        CGPoint P1 = [PP[k+1] CGPointValue];
        CGPoint P2 = [PP[k+2] CGPointValue];
        CGPoint P3 = [PP[k+3] CGPointValue];
        NSArray *segment = [DPMathCurve makeCanonicalSplineSegmentWithPoint0:P0
                                                                       point1:P1
                                                                       point2:P2
                                                                       point3:P3
                                                                  withTension:tension
                                                             withPointsNumber:nr
                                                                    onlyValue:onlyValue
                            ];
        if (onlyValue) {
            [spline[0] addObjectsFromArray:segment[0]];
            [spline[1] addObjectsFromArray:segment[1]];
        }
        else
            [spline addObjectsFromArray:segment];
    }
    
    return spline;
}

+ (NSArray*) prepareCanonicalSegmentsResolution:(NSUInteger)resolution xPoints:(NSArray*)x{
    
    NSMutableArray *xx = [x mutableCopy];
    NSNumber       *maxValue = [xx valueForKeyPath:@"@max.self"];
    
    for (NSUInteger i=0; i<xx.count; i++) {
        double v = [xx[i] doubleValue];
        xx[i]=@(v/[maxValue doubleValue]*(double)resolution);
    }
    
    
    NSMutableArray *n = [NSMutableArray arrayWithCapacity:xx.count-1];
    
    NSUInteger sum=0;
    for (NSUInteger k=0; k<xx.count-1; k++) {
        float distance =  floorf([xx[k+1] floatValue]-[xx[k] floatValue]);
        sum += (NSUInteger)distance;
        n[k] = @(distance);
    }
    
    NSUInteger rest = resolution-sum;
    
    if (rest>0)
        n[0]= @([n[0] integerValue]+rest);
    

    
    return n;
}

+ (NSArray*) makeCanonicalSplineSegmentWithPoint0:(const CGPoint) P0
                                           point1:(const CGPoint) P1
                                           point2:(const CGPoint) P2
                                           point3:(const CGPoint) P3
                                      withTension:(const float) T
                                 withPointsNumber:(NSUInteger)  n
                                        onlyValue:(BOOL)onlyValue
{
    NSMutableArray *segment = [NSMutableArray arrayWithCapacity:n];

    // u vareis b/w 0 and 1.
    // at u=0 cardinal spline reduces to P1.
    // at u=1 cardinal spline reduces to P2.
    
    float   u  = 0;
    CGPoint p  = [DPMathCurve nextCanoicalSplineWithPoint0:P0 point1:P1 point2:P2 point3:P3 withTension:T withU:u];
    float   du = 1.0/n;
    
    
    if (onlyValue){
        [segment addObject:[NSMutableArray new]];
        [segment addObject:[NSMutableArray new]];
    }
    else
        [segment addObject:[NSValue valueWithCGPoint:p]];
    
    for (int k=1; k<=n; k++) {
        u=k*du;
        p = [DPMathCurve nextCanoicalSplineWithPoint0:P0 point1:P1 point2:P2 point3:P3 withTension:T withU:u];
        if (onlyValue){
            [segment[0] addObject:@(p.x)];
            [segment[1] addObject:@(p.y)];
        }
        else
            [segment addObject:[NSValue valueWithCGPoint:p]];
    }
    return segment;
}

//
// compute next point position on canonical curve with source 4 points
//
+ (CGPoint) nextCanoicalSplineWithPoint0:(const CGPoint) P0
                                  point1:(const CGPoint) P1
                                  point2:(const CGPoint) P2
                                  point3:(const CGPoint) P3
                             withTension:(const float) T
                                   withU:(const float) u

{
    float s= (1-T)/2.0f;
    
    //  MC is cardinal matrix
    DPMatrix4 MC = [DPMathMatrix makeMatrix4WithArray:
                    @[
                      @(-s),     @(2.0f-s),   @(s-2.0f),         @(s),
                      @(2.0f*s), @(s-3.0f),   @(3.0f-(2.0f*s)),  @(-s),
                      @(-s),     @(0.0f),     @(s),              @(0.0f),
                      @(0.0f),   @(1.0f),     @(0.0f),           @(0)
                      ]
                    ];
    
    DPVector4 U      = [DPMathVector makeVector4X:pow(u, 3.0f) y:pow(u, 2.0f) z:u w:1.0f];
    DPVector4 Gx     = [DPMathVector makeVector4X:P0.x y:P1.x z:P2.x w:P3.x];
    DPVector4 Gy     = [DPMathVector makeVector4X:P0.y y:P1.y z:P2.y w:P3.y];
    DPVector4 UMC    = [DPMathVector vector:U multiplyMatrix:MC];
    
    return CGPointMake([DPMathVector vector:UMC multiplyVector:Gx], [DPMathVector vector:UMC multiplyVector:Gy]);
}

+ (NSArray*) make2DAdobeWithControls:(NSArray*)controlPoints
                           inXPoints:(NSArray*)xPoints{
    
    NSMutableArray *curve = [NSMutableArray arrayWithCapacity:xPoints.count];
    NSNumber       *maxValue = [xPoints valueForKeyPath:@"@max.self"];

    NSArray *S =  [self splineSlopesWithControlPoints:controlPoints];
    
    for (NSUInteger i=0;i<xPoints.count;i++){
        float x = (float)i;
        float y = [self evaluateAdobeSplineAtPointX:x withControlPoints:controlPoints withSlopes:S];
        if (y<0) y=0;
        if (y>[maxValue floatValue]) y=[maxValue floatValue];
        [curve addObject:[NSValue valueWithCGPoint:CGPointMake(x, y)]];
    }
    return curve;
}


+ (NSArray*) splineSlopesWithControlPoints:(NSArray*)points;
{
    
    // This code computes the unique curve such that:
    //		It is C0, C1, and C2 continuous
    //		The second derivative is zero at the end points

    NSInteger count = points.count;

    double Y[points.count];
    double X[points.count];
    NSMutableArray *S=[NSMutableArray arrayWithCapacity:count];
    NSMutableArray *E=[NSMutableArray arrayWithCapacity:count];
    NSMutableArray *F=[NSMutableArray arrayWithCapacity:count];
    NSMutableArray *G=[NSMutableArray arrayWithCapacity:count];

    for (NSUInteger i=0; i<count; i++) {
        X[i]=[points[i] CGPointValue].x;
        Y[i]=[points[i] CGPointValue].y;
        [S addObject:[NSNumber numberWithFloat:0]];
        [E addObject:[NSNumber numberWithFloat:0]];
        [F addObject:[NSNumber numberWithFloat:0]];
        [G addObject:[NSNumber numberWithFloat:0]];
    }
    
    
    NSInteger start = 0;
    NSInteger end   = count;
    
    double A =  X [start+1] - X [start];
    double B = (Y [start+1] - Y [start]) / A;
    
    
    S [start] = @(B);
    
    NSInteger j=0;
    
    // Slopes here are a weighted average of the slopes
    // to each of the adjcent control points.
    
    for (j = start + 2; j < end; ++j)
    {
        
        double C = X [j] - X [j-1];
        double D = (Y [j] - Y [j-1]) / C;
        
        S [j-1] = @((B * C + D * A) / (A + C));
        
        A = C;
        B = D;
        
    }
    
    S [end-1] = @(2.0 * B - [S [end-2] floatValue]);
    S [start] = @(2.0 * [S [start] floatValue] - [S [start+1] floatValue]);
    
    if ((end - start) > 2)
    {
        
        F [start] = @0.5;
        E [end-1] = @0.5;
        G [start] = @(0.75 * ([S [start] floatValue] + [S [start+1] floatValue]));
        G [end-1] = @(0.75 * ([S [end-2] floatValue] + [S [end-1] floatValue]));
        
        for (j = start+1; j < end - 1; ++j)
        {
            
            A = (X [j+1] - X [j-1]) * 2.0;
            
            E [j] = @((X [j+1] - X [j]) / A);
            F [j] = @((X [j] - X [j-1]) / A);
            G [j] = @(1.5 * [S [j] floatValue]);
            
        }
        
        for (j = start+1; j < end; ++j)
        {
            
            A = 1.0 - [F [j-1] floatValue] * [E [j] floatValue];
            
            if (j != end-1) F [j] = @([F[j] floatValue]/A);
            
            G [j] = @( ([G [j] floatValue] - [G [j-1] floatValue]* [E [j] floatValue]) / A);
            
        }
        
        for (j = end - 2; j >= start; --j)
            G [j] = @([G [j] floatValue] - [F [j] floatValue] * [G [j+1] floatValue]);
        
        for (j = start; j < end; ++j)
            S [j] = G [j];
    }
    
    return S;
}

static double evaluateSplineSegment (double x,
                                     double x0,
                                     double y0,
                                     double s0,
                                     double x1,
                                     double y1,
                                     double s1)
{
    
    double A = x1 - x0;
    
    double B = (x - x0) / A;
    
    double C = (x1 - x) / A;
    
    double D = ((y0 * (2.0 - C + B) + (s0 * A * B)) * (C * C)) +
    ((y1 * (2.0 - B + C) - (s1 * A * C)) * (B * B));
    
    return D;
    
}


+ (float) evaluateAdobeSplineAtPointX:(float)x withControlPoints:(NSArray*)points withSlopes:(NSArray*)S;
{
    
    NSUInteger count = points.count;
    
    // Check for off each end of point list.
    
    if (x <= [points[0] CGPointValue].x )
        return [points[0] CGPointValue].y;
    
    if (x >= [points[count-1] CGPointValue].x)
        return [points[count-1] CGPointValue].y;
    
    // Binary search for the index.
    
    NSUInteger lower = 1;
    NSUInteger upper = count - 1;
    
    while (upper > lower)
    {
        
        NSUInteger mid = (lower + upper) >> 1;
        
        CGPoint point = [points[mid] CGPointValue];
        
        if (x == point.x)
        {
            return point.y;
        }
        
        if (x > point.x)
            lower = mid + 1;
        else
            upper = mid;
        
    }
    
    NSUInteger j = lower;
    
    // X [j - 1] < x <= X [j]
    // A is the distance between the X [j] and X [j - 1]
    // B and C describe the fractional distance to either side. B + C = 1.
    
    // We compute a cubic spline between the two points with slopes
    // S[j-1] and S[j] at either end. Specifically, we compute the 1-D Bezier
    // with control values:
    //
    //		Y[j-1], Y[j-1] + S[j-1]*A, Y[j]-S[j]*A, Y[j]
    
    CGPoint P0 = [points[j-1] CGPointValue];
    CGPoint P1 = [points[j] CGPointValue];
    double  S0 = [S[j-1] doubleValue];
    double  S1 = [S[j] doubleValue];
    return evaluateSplineSegment (x,
                                  P0.x,
                                  P0.y,
                                  S0,
                                  P1.x,
                                  P1.y,
                                  S1);
    
}

@end
