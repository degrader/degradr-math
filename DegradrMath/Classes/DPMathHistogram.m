//
//  DPMathHistogram.m
//  DegradrMath
//
//  Created by denis svinarchuk on 17.11.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

#import "DPMathHistogram.h"
#import "DPMathDistribution.h"
#import <Accelerate/Accelerate.h>
//#import <cblas.h>

@interface DPMathHistogram()
@property (nonatomic,strong) NSMutableArray     *privateCount;
@property (nonatomic,assign) float               gamma;
@end

@implementation DPMathHistogram
{
    NSUInteger        channels;
    float             *tempbuffer;
}

@synthesize size=_size;
@synthesize indexDistribution=_indexDistribution;

- (instancetype) init{
    NSAssert(NO, @"DPMathHistogram constructor should be specified with histogram data: - initWithHistogramData:size:type:");
    return nil;
}

- (instancetype) initWithChannelsType:(DPMathHistogramChannelsType)type size:(NSUInteger)size{
    self = [super init];
    switch (type) {
            
        case DPMathHistogramChannelsType_PLANAR:
            channels = 1;
            _privateCount = [NSMutableArray arrayWithObjects:@(0), nil];
            break;
            
        case DPMathHistogramChannelsType_XYZ:
            _privateCount = [NSMutableArray arrayWithObjects:@(0),@(0),@(0), nil];
            channels = 3;
            break;
            
        case DPMathHistogramChannelsType_XYZW:
            _privateCount = [NSMutableArray arrayWithObjects:@(0),@(0),@(0),@(0), nil];
            channels = 4;
            break;
            
        default:
            NSAssert(NO, @"DPMathHistogram constructor specified with histogram unknown histogram type: %i", type);
            return nil;
    }
    
    _data = DPMathHistogramDataType_UNKNOWN;
    _channelType = type;
    _data = calloc(channels, sizeof(float*));
    _size = size;
    _gamma = 1.0;
    
    return self;
}

- (NSArray*) count{
    return _privateCount;
}

- (DPMathHistogram*)indexDistribution{
    if (!_indexDistribution) {
        _indexDistribution = [DPMathHistogram createIndexDistributionWithChannelType:_channelType size:_size];
    }
    return _indexDistribution;
}

- (void) updateWithData:(uint *)data count:(NSArray*)count{
    
    //
    // accelerated convertion
    //
    int dim = sizeof(uint)/sizeof(unsigned int);
    dim = dim<1?1:dim;
    
    _dataType = DPMathHistogramDataType_NORMAL;
    _privateCount = [count mutableCopy];

    for (NSUInteger c=0; c<channels; c++) {
        if(_data[c]==NULL) _data[c] = calloc(self.size, sizeof(float));
        uint *d = &(data[self.size*c]);
        vDSP_vfltu32((const unsigned int *)d,  dim, (float*)_data[c], 1, self.size);
    }
}

- (void) updateWithData:(uint *)data dataType:(DPMathHistogramDataType)dataType{
    
    //
    // accelerated convertion
    //
    int dim = sizeof(uint)/sizeof(unsigned int);
    dim = dim<1?1:dim;
    
    _dataType = dataType;
    
    for (NSUInteger c=0; c<channels; c++) {
        if(_data[c]==NULL) _data[c] = calloc(self.size, sizeof(float));
        uint *d = &(data[self.size*c]);
        vDSP_vfltu32((const unsigned int *)d,  dim, (float*)_data[c], 1, self.size);
        
        float denom = 0;
        vDSP_sve(_data[c], 1, &denom, self.size);
        
        _privateCount[c] = @(denom);
    }
}


- (DPMathHistogram*) getOutputHistogramWithChannelsType:(DPMathHistogramChannelsType)channelType dataType:(DPMathHistogramDataType)dataType size:(NSUInteger)size{
    if (_outputHistogram) {
        _outputHistogram->_channelType = channelType;
        if (_outputHistogram->_size!=size) {
            [_outputHistogram freeData];
        }
        _outputHistogram->_size = size;
        _outputHistogram->_dataType = dataType;
        return _outputHistogram;
    }
    else{
        DPMathHistogram *hist = [[DPMathHistogram alloc] initWithChannelsType:self.channelType size:self.size];
        hist->_dataType = dataType;
        return hist;
    }
}

- (DPMathHistogram*)createCumulativeHistogram{
    
    DPMathHistogram *hist = [self getOutputHistogramWithChannelsType:self.channelType dataType:DPMathHistogramDataType_CUMULATIVE size:self.size];
    hist->_privateCount = [_privateCount mutableCopy];

    for (NSUInteger c=0; c<channels; c++)
    {
        if(hist->_data[c]==NULL) hist->_data[c] = calloc(hist.size, sizeof(float));
        static float s=1;
        vDSP_vrsum(_data[c], 1, &s, hist->_data[c], 1, hist.size);
    }

    return hist;
}

- (DPMathHistogram*) createPDFWithScale:(float)scale{
    
    if (self.dataType == DPMathHistogramDataType_PDF) {
        return self;
    }

    DPMathHistogram *hist = [self getOutputHistogramWithChannelsType:self.channelType dataType:DPMathHistogramDataType_PDF size:self.size];
    
    for (NSUInteger c=0; c<channels; c++) {
        
        if(hist->_data[c]==NULL) hist->_data[c] = calloc(hist.size, sizeof(float));
        
        float denom = 0;
        
        if (scale==0) {
            //
            // normalize to sum of vector
            //
            vDSP_sve(_data[c], 1, &denom, self.size);
        }
        else{
            vDSP_maxv (_data[c], 1, &denom, self.size);
            denom *=scale;
        }
        vDSP_vsdiv(_data[c], 1, &denom, hist->_data[c], 1, self.size);

        if (scale==0){
            hist->_privateCount[c] = @(1);
        }
        else {
            vDSP_sve(_data[c], 1, &denom, self.size);
            hist->_privateCount[c] = @(denom);
        }
    }
    
    return hist;
}

- (DPMathHistogram*)createCDFWithGamma:(float)gamma withScale:(float)scale{
    
    DPMathHistogram *hist = [self getOutputHistogramWithChannelsType:self.channelType dataType:DPMathHistogramDataType_CDF size:self.size];
    _gamma = gamma;
    
    for (NSUInteger c=0; c<channels; c++)
    {
        if(hist->_data[c]==NULL) hist->_data[c] = calloc(hist.size, sizeof(float));
        
        hist->_privateCount[c] = @(1);
        
        float *os = hist->_data[c];
        float *ds = _data[c];
        
        float sum = 0;
        
        os[0] = 0;
        
        for (NSUInteger i=1; i<hist.size; i++) {
            sum += powf(ds[i],gamma);
            os[i] = sum;
        }
        
        float denom = 0;
        vDSP_maxv (os, 1, &denom, self.size);
        denom *=scale;
        vDSP_vsdiv(os, 1, &denom, os, 1, self.size);
        os[hist.size-1]=1;
        
        vDSP_sve(os, 1, &denom, self.size);
        hist->_privateCount[c] = @(denom);
    }
    
    return hist;
}

- (DPMathHistogram *)specifyHistogram:(DPMathHistogram *)histogram withScale:(float)scale{
    
    DPMathHistogram *referenceCDF;
    
    if (histogram.dataType!=DPMathHistogramDataType_CDF) {
        //
        // get CDF of referenced histogram
        //
        referenceCDF = [histogram createCDFWithGamma:histogram.gamma withScale:scale];
    }
    else
        referenceCDF = histogram;
    
    //
    // get CDF of the source image histogram
    //
    DPMathHistogram *sourceCDF = [self createCDFWithGamma:referenceCDF.gamma withScale:1];
    
    //
    // Normalized LUT to be applied to source image
    //
    DPMathHistogram *lut = [self getOutputHistogramWithChannelsType:self.channelType dataType:DPMathHistogramDataType_LUT size:self.size];
    
    for (NSUInteger c=0; c<lut->channels; c++){
        if(lut->_data[c]==NULL) lut->_data[c] = calloc(lut.size, sizeof(float));

        lut->_privateCount[c] = @(1);
        
        float *os=lut->_data[c];
        
        for (NSUInteger a=0; a<self.size; a++) {
            NSInteger j=self.size-1;
            do{
                lut->_data[c][a] = (float)j/(float)(lut.size-1) * scale;
                j--;
            }while (j>=0 && sourceCDF->_data[c][a]<=referenceCDF->_data[c][j]);
        }
        
        float denom = 0;
        vDSP_maxv (os, 1, &denom, lut->_size);
        denom *=scale;
        vDSP_vsdiv(os, 1, &denom, os, 1, lut->_size);
        
        vDSP_sve(os, 1, &denom, self.size);
        lut->_privateCount[c] = @(denom);
    }
    
    return lut;
}

- (DPMathHistogram*)convolveHistogram:(DPMathHistogram*)filter offset:(NSUInteger)filterOffset withScale:(float)scale {
    DPMathHistogram *hist = [self getOutputHistogramWithChannelsType:self.channelType dataType:DPMathHistogramDataType_LUT size:self.size];

    vDSP_Length halfs = filter.size;
    vDSP_Length asize = hist.size+filter.size*2;
    float addata[asize];

    for (NSUInteger c=0; c<hist->channels; c++){
        
        if(hist->_data[c]==NULL) hist->_data[c] = calloc(hist->_size, sizeof(float));
        
        float *os = &(hist->_data[c][0]);

        //
        // we need to supplement source distribution to apply filter right
        //
        vDSP_vclr(addata, 1, asize);
        
        float zero =  self->_data[c][0];
        vDSP_vsadd(addata, 1, &zero, addata, 1, filter.size);
        
        float one =  self->_data[c][self.size-1];
        vDSP_vsadd(&(addata[hist.size+halfs]), 1, &one, &(addata[hist->_size+halfs]), 1, halfs-1);
        
        float *addr = &(addata[halfs]);
        vDSP_vadd(self->_data[c], 1, addr, 1, addr, 1, self.size);
        
        //
        // apply filter
        //
        vDSP_Length asize = hist.size+filter.size-1;
        vDSP_conv(addata, 1, filter->_data[c], 1, addata, 1, asize, filter.size);

        //
        // normalize coordinates
        //
        addr = &(addata[filterOffset]);
        memcpy(os, addr, hist.size*sizeof(float));

        float left = -os[0];
        vDSP_vsadd(os, 1, &left, os, 1, hist->_size);

        //
        // normalize
        //
        float denom = 0;
        vDSP_maxv (os, 1, &denom, hist.size);
        
        denom *=scale<=0?1:scale;
        vDSP_vsdiv(os, 1, &denom, os, 1, hist->_size);

        vDSP_sve(hist->_data[c], 1, &denom, hist->_size);
        hist->_privateCount[c] = @(denom);
    }
    return hist;
}


- (DPMathHistogram *)addHistogram:(DPMathHistogram *)histogram{

    DPMathHistogram *hist = [self getOutputHistogramWithChannelsType:self.channelType dataType:DPMathHistogramDataType_NORMAL size:self.size];

    for (NSUInteger c=0; c<hist->channels; c++){
        
        if(hist->_data[c]==NULL) hist->_data[c] = calloc(hist.size, sizeof(float));
        
        float *os = hist->_data[c];
        
        vDSP_vadd(self->_data[c], 1, histogram->_data[c], 1, os, 1, hist.size);
        
        float denom = 0;
        vDSP_sve(os, 1, &denom, self.size);
        hist->_privateCount[c] = @(denom);
    }
    
    return hist;
}

- (DPMathHistogram *)multiplyHistogram:(DPMathHistogram *)histogram{
    
    DPMathHistogram *hist = [self getOutputHistogramWithChannelsType:self.channelType dataType:DPMathHistogramDataType_NORMAL size:self.size];
    
    for (NSUInteger c=0; c<hist->channels; c++){
        
        if(hist->_data[c]==NULL) hist->_data[c] = calloc(hist.size, sizeof(float));
        
        float *os = hist->_data[c];
        
        vDSP_vmul(self->_data[c], 1, histogram->_data[c], 1, os, 1, hist.size);
        
        float denom = 0;
        vDSP_sve(os, 1, &denom, self.size);
        hist->_privateCount[c] = @(denom);
    }
    
    return hist;
}

- (float) lowForChannel:(DPMathHistogramChannel)c withClipping:(float)clipping{
    
    float min = 0.0;
    float left = 0.0;
    
    float *hist = self.data[c];
    
    float denom=[self.count[c] floatValue];
    
    if (denom!=1) {
        vDSP_sve(hist, 1, &denom, self.size);
    }

    for (NSUInteger i=1; i<self.size; i++) {
        left += hist[i];
        
        if (left/denom>=clipping) {
            i = i>0?i-1:0;
            min = ((float)i)/(float)(self.size-1);
            break;
        }
    }
    
    return min;
}

- (DPVector2) stddevAndMeanForChannel:(DPMathHistogramChannel)c{
    float  mean;
    float  dev;
    float *C=NULL;
    float *os=self->_data[c];
    float denom=[self.count[c] floatValue];
    if (denom!=1) {
        if (tempbuffer==NULL) tempbuffer = calloc(_size, sizeof(float));
        vDSP_maxv (os, 1, &denom, self->_size);
        vDSP_vsdiv(os, 1, &denom, tempbuffer, 1, self->_size);
        os = tempbuffer;
    }
    vDSP_normalize (os, 1, C, 0, &mean, &dev, self->_size);
    return (DPVector2){mean,dev};
}

- (float) highForChannel:(DPMathHistogramChannel)c withClipping:(float)clipping{
    
    float max =1.0;
    float right = 0.0;
    float *hist = self.data[c];

    NSInteger ms = self.size-1;
    
    float denom=[self.count[c] floatValue];
    
    if (denom!=1) {
        vDSP_sve(hist, 1, &denom, self.size);
    }

    for (NSInteger i=self.size; i>=0; i--) {
        right +=hist[i];
        
        if (right/denom >=clipping) {
            i = i<ms?i+1:ms;
            max = ((float)i)/((float)ms);
            break;
        }
    }
    return max;
}

- (float) meanForChannel:(DPMathHistogramChannel)c{
    float  rOU1;
    
    if (tempbuffer==NULL) tempbuffer = calloc(_size, sizeof(float));

    vDSP_vmul(self->_data[c], 1, self.indexDistribution->_data[c], 1, tempbuffer, 1, self.size);
    vDSP_sve(tempbuffer, 1, &rOU1, self.size);
    
    return rOU1;
}


- (float) maxBinForChannel:(DPMathHistogramChannel)c{
    float *ds=self->_data[c];

    vDSP_Length i;
    float m;
    // skip the first and last
    vDSP_maxvi(&ds[1], 1, &m, &i, self.size-2);

    return (float)i/(float)(self.size);
}

+ (DPMathHistogram*) createNormalDistributionWithFi:(NSNumber*)fi withMu:(NSArray*)mu withSigma:(NSArray*)sigma withChannelsType:(DPMathHistogramChannelsType)channelsType size:(NSUInteger)size{
    
    DPMathHistogram *hist = [[DPMathHistogram alloc] initWithChannelsType:channelsType size:size];
    
    hist->_dataType = DPMathHistogramDataType_NORMAL;
    
    float m = (float)(size-1);
    
    for (NSUInteger c=0; c<hist->channels; c++){

        hist->_privateCount[c] = @(0);

        if(hist->_data[c]==NULL) hist->_data[c] = calloc(hist.size, sizeof(float));
        
        for (NSUInteger i=0; i<hist.size; i++) {
            float v = (float)i/m;
            hist.data[c][i] = 0;
            for (NSUInteger p=0; p<mu.count; p++) {
                hist.data[c][i] += [[DPMathDistribution gaussYPointAtX:@(v) withFi:fi withMu:mu[p] withSigma:sigma[p]] floatValue];
            }
            hist->_privateCount[c]=@([hist->_privateCount[c] floatValue]+hist.data[c][i]);
        }
    }
    return hist;
}

+ (DPMathHistogram*) createIndexDistributionWithChannelType:(DPMathHistogramChannelsType)channelsType size:(NSUInteger)size{
    
    DPMathHistogram *hist = [[DPMathHistogram alloc] initWithChannelsType:channelsType size:size];
    
    hist->_dataType = DPMathHistogramDataType_NORMAL;
    
    float m = (float)(size-1);
    
    for (NSUInteger c=0; c<hist->channels; c++){

        hist->_privateCount[c] = @(0);

        if(hist->_data[c]==NULL) hist->_data[c] = calloc(hist.size, sizeof(float));
        
        for (int i=0; i<size; i++) {
            float v = (float)i/m;
            hist->_data[c][i]=v;
            hist->_privateCount[c]=@([hist->_privateCount[c] floatValue]+v);
        }        
    }
    return hist;
}

- (void) freeData{
    if(_data){
        for (NSUInteger c=0; c<channels; c++) {
            if (_data[c]!=NULL){
                free((void*)_data[c]);
                _data[c] = NULL;
            }
        }
        free(_data);
    }
    _data = NULL;
}

- (void) dealloc{
    [self freeData];
    if (tempbuffer) {
        free(tempbuffer);
    }
}

@end
