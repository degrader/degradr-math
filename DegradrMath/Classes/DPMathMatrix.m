//
//  DPMatrix.m
//  DegradrMath
//
//  Created by denis svinarchuk on 22.09.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

#import "DPMathMatrix.h"

@implementation DPMathMatrix
+ (DPMatrix4) makeMatrix4:(float [16])arrayMatrix{
    DPMatrix4 m = GLKMatrix4Make(
                                 arrayMatrix[0],  arrayMatrix[1],   arrayMatrix[2],   arrayMatrix[3],
                                 arrayMatrix[4],  arrayMatrix[5],   arrayMatrix[6],   arrayMatrix[7],
                                 arrayMatrix[8],  arrayMatrix[9],   arrayMatrix[10],  arrayMatrix[11],
                                 arrayMatrix[12], arrayMatrix[13],  arrayMatrix[14],  arrayMatrix[15]
                                 );
    return GLKMatrix4Transpose(m);
};

+ (DPMatrix4) makeMatrix4WithArray:(NSArray *)arrayMatrix{
    DPMatrix4 m = GLKMatrix4Make(
                                  [arrayMatrix[0] floatValue],  [arrayMatrix[1] floatValue],   [arrayMatrix[2] floatValue],   [arrayMatrix[3] floatValue],
                                  [arrayMatrix[4] floatValue],  [arrayMatrix[5] floatValue],   [arrayMatrix[6] floatValue],   [arrayMatrix[7] floatValue],
                                  [arrayMatrix[8] floatValue],  [arrayMatrix[9] floatValue],   [arrayMatrix[10] floatValue],  [arrayMatrix[11] floatValue],
                                  [arrayMatrix[12] floatValue], [arrayMatrix[13] floatValue],  [arrayMatrix[14] floatValue],  [arrayMatrix[15] floatValue]
                                  );
    return GLKMatrix4Transpose(m);

}
@end
