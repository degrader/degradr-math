//
//  DPMathVector.m
//  DegradrMath
//
//  Created by denis svinarchuk on 22.09.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

#import "DPMathVector.h"

@implementation DPMathVector

+ (DPVector2) normalizeVector2:(DPVector2)vectorIn{
    DPVector2 vector = vectorIn;
    float sum = vector.x+vector.y;
    if (sum==0.0) {
        sum = 1.0;
    }
    vector.x/=sum;
    vector.y/=sum;
    return vector;
}

+ (DPVector3) normalizeVector3:(DPVector3)vectorIn{
    DPVector3 vector = vectorIn;
    float sum = vector.x+vector.y+vector.z;
    if (sum==0.0) {
        sum = 1.0;
    }
    vector.x/=sum;
    vector.y/=sum;
    vector.z/=sum;
    return vector;
}

+ (DPVector4) normalizeVector4:(DPVector4)vectorIn{
    DPVector4 vector = vectorIn;
    float sum = vector.x+vector.y+vector.z+vector.w;
    if (sum==0.0) {
        sum = 1.0;
    }
    vector.x/=sum;
    vector.y/=sum;
    vector.z/=sum;
    vector.w/=sum;
    return vector;
}

+ (DPVector2) makeVector2X:(float)x y:(float)y{
    return GLKVector2Make(x, y);
}

+ (DPVector3) makeVector3X:(float)x y:(float)y z:(float)z{
    return GLKVector3Make(x, y, z);
}

+ (DPVector4) makeVector4X:(float)x y:(float)y z:(float)z w:(float)w{
    return GLKVector4Make(x, y, z, w);
}

+ (DPVector2) makeVector2:(const float [2])vector2{
    return GLKVector2Make(vector2[0], vector2[1]);
}

+ (DPVector3) makeVector3:(const float [3])vector3{
    return GLKVector3Make(vector3[0], vector3[1], vector3[2]);
}

+ (DPVector4) makeVector4:(const float [4])vector4{
    return GLKVector4Make(vector4[0], vector4[1], vector4[2], vector4[3]);
}

+ (DPVector2) makeVector2WithArray:(NSArray *)vector2{
    return GLKVector2Make([vector2[0] floatValue], [vector2[1] floatValue]);
}

+ (DPVector3) makeVector3WithArray:(NSArray *)vector3{
    return GLKVector3Make([vector3[0] floatValue], [vector3[1] floatValue], [vector3[2] floatValue]);
}

+ (DPVector4) makeVector4WithArray:(NSArray *)vector4{
    return GLKVector4Make([vector4[0] floatValue], [vector4[1] floatValue], [vector4[2] floatValue], [vector4[3] floatValue]);
}

+ (DPVector4) vector:(DPVector4)vectorLeft multiplyMatrix:(DPMatrix4)matrixRight{
    DPVector4 vectorRight = vectorLeft;
    DPMatrix4 matrixLeft  = GLKMatrix4Transpose(matrixRight);
    return GLKMatrix4MultiplyVector4(matrixLeft, vectorRight);
}

+ (float) vector:(DPVector4)columnVector multiplyVector:(DPVector4)rowVector{
    return GLKVector4DotProduct(columnVector, rowVector);
}

@end
