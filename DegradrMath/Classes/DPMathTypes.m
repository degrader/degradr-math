//
//  DPMathTypes.m
//  DegradrMath
//
//  Created by denis svinarchuk on 22.09.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

#import "DPMathTypes.h"


@implementation DPMatrix
{
    NSMutableArray *matrix;
}

- (instancetype) initWithArray:(NSArray *)array columns:(NSUInteger)columns rows:(NSUInteger)rows{
    self = [super init];
    if (self) {
        _columns = columns;
        _rows = rows;
        matrix = [NSMutableArray arrayWithCapacity:_columns*rows];
        NSUInteger index=0;
        for (NSUInteger xi=0; xi<columns; xi++) {
            for (NSUInteger yi=0; yi<rows; yi++) {
                if (index<array.count)
                    [matrix addObject:array[index]];
                else
                    [matrix addObject:@(0)];
                
                index++;
            }
        }
    }
    return self;
}

- (NSArray*) columnAtIndex:(NSUInteger)index{
    NSMutableArray *column = [NSMutableArray arrayWithCapacity:self.rows];
    for (NSUInteger i=0; i<self.rows; i++) {
        [column addObject:matrix[i*self.columns+index]];
    }
    return column;
}

- (NSArray*) rowAtIndex:(NSUInteger)index{
    NSMutableArray *row = [NSMutableArray arrayWithCapacity:self.columns];
    for (NSUInteger i=0; i<self.columns; i++) {
        [row addObject:matrix[self.columns*index+i]];
    }
    return row;
}

- (NSString*)description{
    NSMutableString *s=[NSMutableString stringWithString:@"["];
    NSUInteger i=0;
    for (NSUInteger yi=0; yi<self.rows; yi++) {
        NSArray *row = [self rowAtIndex:yi];
        NSUInteger ci = 0;
        for (id obj in row) {
            if (i>0) {
                [s appendString:@" "];
            }
            i++;
            if ([obj isKindOfClass:[NSNumber class]]) {
                [s appendFormat:@"%2.4f", [obj floatValue]];
            }
            else
                [s appendFormat:@"%@", obj];
            if (i<matrix.count) {
                if (ci<self.columns-1) {
                    [s appendFormat:@","];
                }
                else
                    [s appendFormat:@";"];
            }
            ci++;
        }
        if (yi<self.rows-1)
            [s appendString:@"\n"];
    }
    [s appendString:@"]"];
    return s;
}

@end