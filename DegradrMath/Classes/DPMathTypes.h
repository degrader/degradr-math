//
//  DPMathTypes.h
//  DegradrMath
//
//  Created by denis svinarchuk on 22.09.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <Accelerate/Accelerate.h>
#include <GLKit/GLKMath.h>

typedef GLKVector2 DPVector2;
typedef GLKVector3 DPVector3;
typedef GLKVector4 DPVector4;

typedef GLKMatrix2 DPMatrix2;
typedef GLKMatrix3 DPMatrix3;
typedef GLKMatrix4 DPMatrix4;


@interface DPMatrix: NSObject
@property (nonatomic,readonly) NSUInteger columns;
@property (nonatomic,readonly) NSUInteger rows;
- (instancetype) initWithArray:(NSArray *)array columns:(NSUInteger)columns rows:(NSUInteger)rows;
- (NSArray*)columnAtIndex:(NSUInteger)index;
- (NSArray*)rowAtIndex:(NSUInteger)index;
@end
