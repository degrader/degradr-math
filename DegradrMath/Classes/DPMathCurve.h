//
//  DPSpline.h
//  DegradrMath
//
//  Created by denis svinarchuk on 22.09.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DPMathMatrix.h"
#import "DPMathVector.h"

@interface DP3DCurve: NSObject
@property (nonatomic,strong) NSMutableArray *xPoints;
@property (nonatomic,strong) NSMutableArray *yPoints;
@property (nonatomic,strong) NSMutableArray *zMatrix;
- (instancetype) initWithXPoints:(NSArray*)xPoints withYPoints:(NSArray*)yPoints withZMatrix:(NSArray*)zMatrix;
@end

@interface DPMathCurve : NSObject
/**
 * Make 2D unifrom curve with control points, resolution and tension. Resolution is number of points in output points array.
 * With tension == 0 it's create Catmull-Rom spline.
 */
+ (NSArray*) make2DUniformWithPoints:(NSArray*)points
                            withSize:(NSUInteger)size
                         withTension:(float)tension;


+ (NSArray*) make2DAdobeWithControls:(NSArray*)controlPoints
                                inXPoints:(NSArray*)xPoints;


/**
 * 2D piecewise cubic Hermite spline.
 * It interpolates to find Y, the values of the underlying function Y at the controlPoints in the array xPoints,
 * using piecewise cubic Hermite splines. controlPoints must be vectors of length N.
 * CatmullRom specifies spline interpolation within segments.
 */
+ (NSArray*) make2DCatmullRomWithControls:(NSArray*)controlPoints
                            inXPoints:(NSArray*)xPoints;

+ (NSArray*) make2DCatmullRomWithControls:(NSArray*)controlPoints
                                 withSize:(NSUInteger)size
                             withInterval:(float)interval;

/**
 * 3D piecewise cubic Hermite spline.
 * It interpolates to find Z, the values of the underlying function Y at the controlPoints in the array xPoints and yPoints,
 * using piecewise cubic Hermite splines. controlPoints must be vectors of length N.
 * CatmullRom specifies spline interpolation within segments.
 */
+ (DP3DCurve*) make3DCatmullRomWithCurveControls:(DP3DCurve*)curveControls inXPoints:(NSArray *)xPoints inYPoints:(NSArray *)yPoints;

@end
