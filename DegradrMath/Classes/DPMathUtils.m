//
//  DPMathUtils.m
//  DegradrMath
//
//  Created by denis svinarchuk on 26.09.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

#import "DPMathUtils.h"

float clamp(float v, float minval, float maxval){
    return fmin(fmax(v, minval), maxval);
}

float mix (float x, float y, float a){
    return (x + (y - x) * a);
}


float sign(float v){
    if (v<0) { return -1;}
    if (v>0) { return  1;}
    return 0;
}
