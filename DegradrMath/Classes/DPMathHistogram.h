//
//  DPMathHistogram.h
//  DegradrMath
//
//  Created by denis svinarchuk on 17.11.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DPMath.h"

typedef enum {
    DPMathHistogramDataType_UNKNOWN,
    DPMathHistogramDataType_NORMAL,
    DPMathHistogramDataType_PDF,
    DPMathHistogramDataType_CUMULATIVE,
    DPMathHistogramDataType_CDF,
    DPMathHistogramDataType_LUT
}DPMathHistogramDataType;

typedef enum {
    DPMathHistogramChannelsType_PLANAR = 1,
    DPMathHistogramChannelsType_XYZ    = DPMathHistogramChannelsType_PLANAR<<1,
    DPMathHistogramChannelsType_XYZW   = DPMathHistogramChannelsType_XYZ<<1
}DPMathHistogramChannelsType;

typedef enum {
    DPMathHistogramChannel_X=0,
    DPMathHistogramChannel_Y=1,
    DPMathHistogramChannel_Z=2,
    DPMathHistogramChannel_W=3
}DPMathHistogramChannel;



/**
 * Common histogram math presentations and histogram operations class.
 * Histogram can contain normal histogram bins or discrete Cumulative Distribution (i.e. discrete CDF presentation).
 */
@interface DPMathHistogram : NSObject

/**
 * Histogram size, i.e. number of bins of number of pixels with certain intensity.
 */
@property (nonatomic,readonly) NSUInteger          size;

/**
 * Histogram current bins count for every channels. If data type is CFD or PDF count is 1.
 */
@property (nonatomic,readonly) NSArray             *count;

/**
 * Index distribution histogram for the instance. It depends on the histogram size only.
 */
@property (nonatomic,readonly) DPMathHistogram     *indexDistribution;

/**
 * Histogram data type defines what number types lay in their points, it can the follows:
 * 1. integer count of bins, normal
 * 2. normalized probabilities distribution (PDF)
 * 3. integer cumulative count of bins
 * 4. normalized cumulative distribution of the histogram (CDF)
 * 5. mapping table (LUT)
 */
@property (nonatomic,readonly)   DPMathHistogramDataType dataType;

/**
 * Histogram channels type describes what type of image the histogram presents: one channel (gray), 3-channels (RGB,Lab,HSV,YUV,YCbCr, etc..) or 3-channles type
 * with addidional channel like alpha-channel uses RGBA image presentation.
 */
@property (nonatomic,assign)   DPMathHistogramChannelsType channelType;

/**
 * Table contains histograms, mapping function of pixels, or PDF/CDF. Every type of data is computed per input channel.
 */
@property (nonatomic,readonly) float **data;

/**
 * Create a new histogram has certain channales and entries size;
 */
- (instancetype) initWithChannelsType:(DPMathHistogramChannelsType)type size:(NSUInteger)size;

/**
 * Update histogram with data contains total counts of bins. Data must be DPMathHistogramDataType_NORMAL.
 * Count of bins does not involved in histogram operation just informs how many. It can be used in future 
 * processing in custom extentions.
 */
- (void) updateWithData:(uint*)data count:(NSArray*)count;

/**
 * Update histogram with data with certain type.
 */
- (void) updateWithData:(uint*)data dataType:(DPMathHistogramDataType)dataType;

/**
 * Create new cumulative presentation of the histogtam
 */
- (DPMathHistogram*) createCumulativeHistogram;

/**
 * Create new discrete probabilities distribution function of the histogtam when the scale is equal to 0.
 * When a scale!=0 normalize the distribution by max value (of the distribution) to the scale factor.
 */
- (DPMathHistogram*) createPDFWithScale:(float)scale;

/**
 * Creat new discrete comulative distribution function of the histogram with gamma correction.
 */
- (DPMathHistogram*)createCDFWithGamma:(float)gamma withScale:(float)scale;

/**
 * Specify histogram by another to LUT table.
 * @param histogram is a desired hsitogram
 * @return new table of pixel mapping function (LUT)
 */
- (DPMathHistogram*) specifyHistogram:(DPMathHistogram*)histogram withScale:(float)scale;

/**
 * Add another histogram to the histogram create new. New histogram is not normalized.
 * @param histogram is a desired hsitogram
 * @return new presentation of two stacked histograms
 */
- (DPMathHistogram *)addHistogram:(DPMathHistogram *)other;

/**
 * Create new multiplied histogram from the histogram and other. New histogram is not normalized.
 */
- (DPMathHistogram *)multiplyHistogram:(DPMathHistogram *)other;

/**
 * Apply confoltion to the histogram and create new with input filter histogram. 
 * New histogram is normalized to scale.
 */
- (DPMathHistogram*)convolveHistogram:(DPMathHistogram*)filter offset:(NSUInteger)filterOffset withScale:(float)scale;

/**
 * Get minimum intensity (left ). Histogram must not be cumulative.
 */
- (float) lowForChannel:(DPMathHistogramChannel)channel withClipping:(float)clipping;

/**
 * Get the maximum of histogram. Histogram must not be cumulative.
 */
- (float) highForChannel:(DPMathHistogramChannel)channel withClipping:(float)clipping;

/**
 * Get avarage value for the channel. Histogram must not be cumulative/CDF.
 */
- (float) meanForChannel:(DPMathHistogramChannel)channel;

/**
 * Get position of maximum bin. Histogram must not be cumulative/PDF/CDF.
 */
- (float) maxBinForChannel:(DPMathHistogramChannel)channel;

- (DPVector2) stddevAndMeanForChannel:(DPMathHistogramChannel)channel;

/**
 * Create a histogram with sequential sum of normal distributions.
 */
+ (DPMathHistogram*) createNormalDistributionWithFi:(NSNumber*)fi withMu:(NSArray*)mu withSigma:(NSArray*)sigma withChannelsType:(DPMathHistogramChannelsType)channelsType size:(NSUInteger)size;

/**
 * Set static output hsitogram for hisogram operations. In case any static ouput is not set, every operations create new histogram instance.
 */
@property (nonatomic,strong) DPMathHistogram *outputHistogram;

@end
