//
//  DPMathVector.h
//  DegradrMath
//
//  Created by denis svinarchuk on 22.09.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

#import "DPMathTypes.h"

@interface DPMathVector : NSObject

+ (DPVector2) makeVector2X:(float)x y:(float)y;
+ (DPVector3) makeVector3X:(float)x y:(float)y z:(float)z;
+ (DPVector4) makeVector4X:(float)x y:(float)y z:(float)z w:(float)w;

+ (DPVector2) makeVector2:(const float[2])vector2;
+ (DPVector3) makeVector3:(const float[3])vector3;
+ (DPVector4) makeVector4:(const float[4])vector4;

+ (DPVector2) makeVector2WithArray:(NSArray*)vector2;
+ (DPVector3) makeVector3WithArray:(NSArray*)vector3;
+ (DPVector4) makeVector4WithArray:(NSArray*)vector4;

+ (DPVector4) vector:(DPVector4)vectorLeft   multiplyMatrix:(DPMatrix4)matrixRight;
+ (float)     vector:(DPVector4)columnVector multiplyVector:(DPVector4)rowVector;

+ (DPVector2) normalizeVector2:(const DPVector2)vector;
+ (DPVector3) normalizeVector3:(const DPVector3)vector;
+ (DPVector4) normalizeVector4:(const DPVector4)vector;

@end
