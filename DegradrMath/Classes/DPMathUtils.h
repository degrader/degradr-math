//
//  DPMathUtils.h
//  DegradrMath
//
//  Created by denis svinarchuk on 26.09.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

#import <Foundation/Foundation.h>

extern float clamp(float v, float minval, float maxval);
extern float  mix (float x, float y, float a);
extern float sign(float v);
