//
//  DPMathDistribution.h
//  DegradrMath
//
//  Created by denis svinarchuk on 25.09.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DPMathTypes.h"

@interface DPMathDistribution : NSObject
+ (NSNumber*) gaussYPointAtX:(NSNumber*)x withFi:(NSNumber*)fi withMu:(NSNumber*)mu withSigma:(NSNumber*)sigma;
+ (NSNumber*) gauss2YPointAtX:(NSNumber*)x withFi:(NSNumber*)fi withMu:(NSArray*)mu withSigma:(NSArray*)sigma;
+ (NSArray*)  normal1DsWeightsWithPoints:(NSArray *)xPoints withFi:(NSNumber*)fi withMu:(NSNumber*)mu withSigma:(NSNumber*)sigma;
@end
