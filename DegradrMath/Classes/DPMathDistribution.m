//
//  DPMathDistribution.m
//  DegradrMath
//
//  Created by denis svinarchuk on 25.09.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

#import "DPMathDistribution.h"

@implementation DPMathDistribution

+ (double) gaussDoubleYPointAtX:(double)x withFi:(double)fi withMu:(double)mu withSigma:(double)sigma{
    return fi * exp(- (pow((x-mu),2)) / (2*pow(sigma, 2)));
}

+ (NSNumber*) gaussYPointAtX:(NSNumber*)x withFi:(NSNumber*)fi withMu:(NSNumber*)mu withSigma:(NSNumber*)sigma{
    return [NSNumber numberWithDouble: [self gaussDoubleYPointAtX:[x floatValue] withFi:[fi floatValue] withMu:[mu floatValue] withSigma:[sigma floatValue]]];
}

+ (NSNumber*) gauss2YPointAtX:(NSNumber*)x withFi:(NSNumber*)fi withMu:(NSArray*)mu withSigma:(NSArray*)sigma{
    
    double c1 = (x<=mu[0])?1.0f:0.0f;
    double c2 = (x>=mu[1])?1.0f:0.0f;
    
    double y1 = [self gaussDoubleYPointAtX:[x doubleValue] withFi:[fi doubleValue] withMu:[mu[0] doubleValue] withSigma:[sigma[0] doubleValue]] * c1 + (1.0-c1);
    double y2 = [self gaussDoubleYPointAtX:[x doubleValue] withFi:[fi doubleValue] withMu:[mu[1] doubleValue] withSigma:[sigma[1] doubleValue]] * c2 + (1.0-c2);
    
    return  [NSNumber numberWithDouble:y1*y2];
}

+ (NSArray*) normal1DsWeightsWithPoints:(NSArray *)xPoints withFi:(NSNumber*)fi withMu:(NSNumber*)mu withSigma:(NSNumber*)sigma{
    NSMutableArray *weights=[NSMutableArray arrayWithCapacity:xPoints.count];
    for (NSNumber *x in xPoints) {
        [weights addObject:[self gaussYPointAtX:x withFi:fi withMu:mu withSigma:sigma]];
    }
    return weights;
}

@end
