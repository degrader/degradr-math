//
//  DPMatrix.h
//  DegradrMath
//
//  Created by denis svinarchuk on 22.09.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

#import "DPMathTypes.h"

@interface DPMathMatrix : NSObject
+ (DPMatrix4) makeMatrix4:(float[16])arrayMatrix;
+ (DPMatrix4) makeMatrix4WithArray:(NSArray*)arrayMatrix;
@end
