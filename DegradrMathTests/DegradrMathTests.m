//
//  DegradrMathTests.m
//  DegradrMathTests
//
//  Created by denis svinarchuk on 22.09.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DPMath.h"

@interface DegradrMathTests : XCTestCase

@end

@implementation DegradrMathTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testVectorOperations {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.

    DPVector4 v = [DPMathVector makeVector4X:8 y:4 z:2 w:1];
    
    float s= 1.0f/2.0f;
    //  MC is cardinal matrix
    NSArray *m = @[
                   @(-s),     @(2.0f-s),   @(s-2.0f),         @(s),
                   @(2.0f*s), @(s-3.0f),   @(3.0f-(2.0f*s)),  @(-s),
                   @(-s),     @(0.0f),     @(s),              @(0.0f),
                   @(0.0f),   @(1.0f),     @(0.0f),           @(0)
                   ];

    DPMatrix4 MC  = [DPMathMatrix makeMatrix4WithArray:m];
    DPVector4 vt  = [DPMathVector vector:v multiplyMatrix:MC];
    DPVector4 UMC = [DPMathVector vector:v multiplyMatrix:MC];
    
    XCTAssert(vt.x==-1.000000 && vt.y==3.000000 && vt.z== -3.000000 && vt.w==2.000000,"vector multiplyMatrix mistmatch");

    DPVector4 Gx  = [DPMathVector makeVector4X:10 y:2 z:3 w:4];
    float     p   = [DPMathVector vector:UMC multiplyVector:Gx];
    
    XCTAssert(p==-5,"multiplyVector mistmatch: %f",p);
}

- (void) printDistribution:(NSArray*)spline{
    
    printf("\n");
    
    for (NSUInteger k=0; k<spline.count; k++) {
        NSMutableString *value = [NSMutableString stringWithFormat:@""];
        
        id     sp = spline[k];
        float  vi=0;
        if (![sp isKindOfClass:[NSNumber class]]) {
            CGPoint  p  = [sp CGPointValue];
            vi = p.y /255.0 * 100;
        }
        else
            vi = [sp floatValue];
        
        for (int i=0; i<vi; i++) {
            [value appendString:@"."];
        }
        
        if (![sp isKindOfClass:[NSValue class]]) {
            CGPoint  p  = [sp CGPointValue];
            printf(" ***[%4lu] [%4.1f,%4.3f] %s\n",(unsigned long)k, p.x, p.y, [value UTF8String]);
        }
        else
            printf(" ***[%4lu] [%4.1f] %s\n",(unsigned long)k, [sp floatValue], [value UTF8String]);
    }
}

- (void)test2DCurve {
    
    NSArray *defaultCurve = [NSArray arrayWithObjects:
                             [NSValue valueWithCGPoint:CGPointMake(  0,  10)],
                             [NSValue valueWithCGPoint:CGPointMake( 50,  10)],
                             [NSValue valueWithCGPoint:CGPointMake(128, 128)],
                             [NSValue valueWithCGPoint:CGPointMake(170,   2)],
                             [NSValue valueWithCGPoint:CGPointMake(255, 255)],
                             nil];
    
    NSArray *spline = [DPMathCurve make2DCatmullRomWithControls:defaultCurve withSize:255 withInterval:1];
        
    XCTAssert([spline[170] CGPointValue].y==2.0, "DPMathCurve error spline[170]=%f",[spline[170] CGPointValue].y);
}

- (void)testDPMatrix {
    NSArray  *m = @[
                   @(11),@(12),@(13),@(14),@(15),
                   @(21),@(22),@(23),@(24),@(25),
                   @(31),@(32),@(33),@(34),@(35),
                   @(41),@(42),@(43),@(44),@(45),
                   @(51),@(52),@(53),@(54),@(55),
                   @(61),@(62),@(63),@(64),@(65),
                   @(71),@(72),@(73),@(74),@(75)
                   ];
    DPMatrix *matrix = [[DPMatrix alloc] initWithArray:m columns:5 rows:7];
    
    XCTAssert([[matrix columnAtIndex:2][1] integerValue]==23,"DPMatrix column error...");
    XCTAssert([[matrix rowAtIndex:2][1] integerValue]==32,"DPMatrix row error...");
}

- (void)test3DSpline{
    float xn  = 20;
    float yn  = 10;
    float dx  = xn/360;
    float dy  = yn;
    NSArray *x = @[ @0,   @(20.14*dx),   @(65.0*dx),   @(164.25*dx),  @(185.0*dx),  @(237.53*dx), @(360*dx)];
    NSArray *y = @[ @0.0, @(0.1*dy),     @(0.3*dy),    @(0.6*dy),     @(0.8*dy),    @(1.0*dy) ];
    NSArray *zm= @[
                   @0.1,  @0.1,     @0.1,    @0.1,     @0.2,     @0.7,    @0.1,   // black   0.0
                   @0.4,  @0.3,     @0.2,    @0.4,     @0.5,     @0.9,    @0.4,   // shadows 0.2
                   @0.6,  @0.5,     @0.4,    @0.7,     @0.8,     @1.2,    @0.6,   // greys   0.4
                   @0.7,  @0.8,     @0.7,    @1.0,     @1.0,     @0.8,    @0.7,   // mid     0.6
                   @0.8,  @0.8,     @1.2,    @0.7,     @0.9,     @0.4,    @0.8,   // lights  0.8
                   @0.2,  @0.2,     @0.5,    @0.2,     @0.2,     @0.1,    @0.2
                   ];
    
    NSMutableArray *ehues = [NSMutableArray arrayWithCapacity:xn];
    NSMutableArray *lumas = [NSMutableArray arrayWithCapacity:yn];
    
    for (NSUInteger i=0; i<=xn; i++) {
        [ehues addObject:[NSNumber numberWithFloat:i]];
    }

    for (NSUInteger i=0; i<=yn; i++) {
        [lumas addObject:[NSNumber numberWithFloat:i]];
    }

    DP3DCurve *curveControles = [[DP3DCurve alloc] initWithXPoints:x withYPoints:y withZMatrix:zm];
    
    DP3DCurve *curve = [DPMathCurve make3DCatmullRomWithCurveControls:curveControles inXPoints:ehues inYPoints:lumas];
    int v =  (int)([curve.zMatrix[4] floatValue] * 10000.0f);
    XCTAssert( v == 996, "DP3DCurve error zMatrix value... curve.zMatrix[4] = %@", curve.zMatrix[4]);
    
    //NSLog(@"3DSpline MATLAB code generator, check result copy and paste to MATLAB console \n%@", curve);

}

- (void)test1DNormalDistribution{
    NSMutableArray *xPoints = [NSMutableArray new];
    for (NSUInteger i=0; i<256; i++) {
        [xPoints addObject:[NSNumber numberWithInteger:i]];
    }
    
    NSArray *y = [DPMathDistribution normal1DsWeightsWithPoints:xPoints withFi:@100.0 withMu:@50 withSigma:@20];

    //[self printDistribution:y];
    XCTAssert([y[50] integerValue]==100, "NormalDistribution error..");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
