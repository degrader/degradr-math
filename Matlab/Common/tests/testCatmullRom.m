hold off; 

X = [0, 50,128,172,255]; 
Y = [10,10,128,200,255]; 

xcn = 0:1:255;
ycn = catmullromSpline2D(X,Y,xcn); 

plot(xcn,ycn,'b','LineWidth',1); 
grid on; 
axis([0,256, 0, 256])