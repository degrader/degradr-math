hold off; 

X = [0,50,128,172,255]; 
Y = [0,10,128,129,255]; 
xstd = 0:1:255;
ystd = interp1(X,Y,xstd,'PCHIP'); 

plot(xstd,ystd,'g','LineWidth',1); 
grid on; 
axis([0,256, 0, 256])