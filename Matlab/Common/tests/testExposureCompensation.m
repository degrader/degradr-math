%%
%

clf
exposure=1; 
white = 1.0 / power (2.0, max (0.0, exposure)); 
black=0.001; 
black=min(black,0.99*white);

hold on; 
x=0:1:256; 
y=dng_exposure(x,exposure)*max(x);
%y=gamma_exposure(x,1.8,exposure);
plot(x,y); 
axis([0 256 0 256]); 
grid on;


y=dng_exposure(x,-exposure)*max(x);
plot(x,y); 
axis([0 256 0 256]); 
grid on;


% rx=0:1:256; 
% ry=dng_exposure_ramp(x,white,black,black); 
% plot(rx,ry); axis([0 256 0 1]); grid on;

