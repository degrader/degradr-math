hold off; 

X = [0,50,128,172,255]/255; 
Y = [0,20,128,200,255]/255; 

[xcr,ycr] = cardinalSpline2D(X,Y,256,0); 

xcr = xcr*255;
ycr = ycr*255;

plot(xcr,ycr,'r','LineWidth',1); 
grid on; 
axis([0,256, 0, 256]);
