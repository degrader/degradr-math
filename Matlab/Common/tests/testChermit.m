hold off; 

X = [0,50,128,172,255]; 
Y = [0,20,128,200,255]; 
XI= 0:1:255;
YP= ones(1,255);

[ych,ypi] = chermite(X,Y,YP,XI,0); 

plot(XI,ych,'b','LineWidth',1); 
grid on; 
axis([0,256, 0, 256])