%%
%
function y = shadows_weights(x, weight, width, ascent)
    maxe = shadows_weight_internal(0, weight, width, ascent);
    y = weight * shadows_weight_internal(x, weight, width, ascent)/maxe;
end

function y = shadows_weight_internal(x, weight, width, ascent)
    w = width;
    a = ascent;
    y = weight./exp(x.*a/w)*(w);
end

