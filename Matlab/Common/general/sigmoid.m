%% g = sigmoid(z) - function is a mathematical function having an "S" shape (sigmoid curve).
% Often, sigmoid function refers to the special case of the logistic function shown in the first figure and defined by the formula
% S(t) = 1/(1 + e(-t)).
%

function g = sigmoid(z)
    g = 1.0 ./ (1.0 + exp(-z));
end
