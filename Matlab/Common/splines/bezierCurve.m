%% bezierCurve(N, P)   constructs a Bezier curve from given control points. 
% P is a vector of control points. N is the number of points to calculate. 
%

function [x, y] = bezierCurve(N, P) 


    Np = size(P, 1); 
    u = linspace(0, 1, N); 
    B = zeros(N, Np); 
    
    for i = 1:Np 
        B(:,i) = nchoosek(Np-1,i-1).*u.^(i-1).*(1-u).^(Np-i); %B is the Bernstein polynomial value 
    end 
    
    S = B*P; 
    x = S(:, 1); 
    y = S(:, 2);
    
end