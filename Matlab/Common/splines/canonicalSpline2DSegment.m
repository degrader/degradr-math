%% canonicalSplineSegment(P0,P1,P2,P3,tension) - create canonical spline interpolation segment for integers control point.
%
%

function [x,y] = canonicalSpline2DSegment(P0,P1,P2,P3,tension)
    
    X0 = P1(1);
    X1 = P1(1);
    X2 = P2(1);
    X3 = P1(1);

    Y0 = P0(2);
    Y1 = P1(2);
    Y2 = P2(2);
    Y3 = P3(2);

    n = (X2-X1)-1;
    
    x(:,1) = X1;
    xs(:,1)=x(1);
    du = 1/(n);
    ys(:,1) = canonicalSplinePoint(Y0,Y1,Y2,Y3,du,tension);                
    y(:,1) = ys(1);
    
    for k=1:n
        p = k*du;
        X = x(k)+1;
        Xs = canonicalSplinePoint(X0,X1,X2,X3,p,tension);                
        Ys = canonicalSplinePoint(Y0,Y1,Y2,Y3,p,tension);                
        
        % need to find Y
        
        x(:,k+1) = X;
        xs(:,k+1) = Xs;
        ys(:,k+1) = Ys; 
        y(:,k+1) = Ys;
        fprintf('  k = %i du = %f  p = %f x=%f = %f, y=%f =%f\n',k,du,p,x(:,k+1),Xs,ys(:,k+1),Ys)
    end   
    
%     for k=1:length(ys)-1
%         y1 = ys(k);
%         x1 = xs(k);
%         y2 = ys(k+1);
%         x2 = xs(k+1);
%         Y = y1+(y2-y1)/(x2-x1)*(x(k)-x1);
%         y(:,k+1) = Y;
%         fprintf(' k[%i] [%f,%f] == [%f,%f]\n',k,x1,y1, x(k),Y);
%     end
%     y(end) = ys(end);
end