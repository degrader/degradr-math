%% canonicalSplinePoint(P0,P1,P2,P3,p,tension) - return interpolated point with given
% 4 control points: 'P0','P1','P2','P3', tension 'tension' and arbitrary point 'p'.
% Arbitrary point must be normolized to integer.
%

function y = canonicalSplinePoint(P0,P1,P2,P3,p,tension)

    s= (1-tension)./2;
    
    % MC is cardinal matrix
    MC=[-s     2-s   s-2        s;
        2.*s   s-3   3-(2.*s)   -s;
        -s     0     s          0;
        0      1     0          0];

    Gy  = [P0;P1;P2;P3];
    PMC = [p^3  p^2  p  1]*MC;    
    y   = PMC*Gy;
    
end