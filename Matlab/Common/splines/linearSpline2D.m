%%
%

function [X,Y] = linearSpline2D(x,y,resolution)
    [X,Y] = cardinalSpline2D(x,y,resolution,1);
end