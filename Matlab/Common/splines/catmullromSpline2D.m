%% catmullromSpline2D(x,y,xi) - make curve with cubic Hermite polynomial catMullRom splined
%

function curve = catmullromSpline2D(x,y,xi)
    
    n = length(x);
    curve = zeros(length(xi),1);
    
    for i = 1:length(xi)
        
       Xi= xi(i);
       [k1,k2] = findCloseControls(x,Xi,n);                           
       
       [a,b,h] = catMullRomSpline(x,y,n,k1,k2);
       
       % Evaluate cubic Hermite polynomial
       t = (Xi - x(k1))/h;
       t2 = t*t;
       t3 = t2*t;

       h00 =  2*t3 - 3*t2 + 1;
       h10 =    t3 - 2*t2 + t;
       h01 = -2*t3 + 3*t2;
       h11 =    t3 - t2;
       
       curve(i) = h00*y(k1) + h10*h*a + h01*y(k2) + h11*h*b;
    end
    
end

%%
% eval catMullSpline coeff
%
function [a,b,h] = catMullRomSpline(x,y,n,k1,k2)
    h = x(k2) - x(k1);

    if (k1 == 1)
     a = (y(k2) - y(k1))/h;
     b = (y(k2+1) - y(k1))/(x(k2+1) - x(k1));
    elseif (k2 == n)
     a = (y(k2) - y(k1-1))/(x(k2) - x(k1-1));
     b = (y(k2) - y(k1))/h;
    else
     a = (y(k2) - y(k1-1))/(x(k2) - x(k1-1));
     b = (y(k2+1) - y(k1))/(x(k2+1) - x(k1));
    end
end

%%
%
function [k1,k2] = findCloseControls(x,xi,n)
    k1 = 1;
    k2 = n;
    while (k2-k1 > 1)
      k = floor((k2+k1)/2.0);
      if (x(k) > xi)
         k2 = k;
      else
         k1 = k;
      end
    end
end

%%
% end