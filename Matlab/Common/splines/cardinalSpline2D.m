%%
%

function [X,Y] = cardinalSpline2D(x,y,resolution,t)
        
    Px = [x(1), x(1:end), x(end)];
    Py = [y(1), y(1:end), y(end)];        
    
    X = [];
    Y = [];
    
    nn  = cardinal_resulution(x,resolution);
    
    for k=1:length(Px)-3
        nr = nn(k)-1;
        [XX,YY] = cardinal_interval([Px(k),Py(k)],[Px(k+1),Py(k+1)],[Px(k+2),Py(k+2)],[Px(k+3),Py(k+3)],t,nr);        
        X = [X, XX];
        Y = [Y, YY];
    end
    
end

function n = cardinal_resulution(x,resolution)

    xx = x;
    xx = (xx/max(xx))*resolution;

    n = zeros(length(xx)-1,1);
    for k=1:length(x)-1
        n(k) = (xx(k+1)-xx(k));
    end
    
    n = floor(n);
    rest = resolution-sum(n);
        
    if rest>0
    n(1)=n(1)+rest;
    end        
end

function [x,y] = cardinal_interval(P0,P1,P2,P3,T,n)

    % u vareis b/w 0 and 1.
    % at u=0 cardinal spline reduces to P1.
    % at u=1 cardinal spline reduces to P2.

    u=0;
    [x(:,1),y(:,1)]=eval_cardinal(P0,P1,P2,P3,T,u); % interval(:,1)=length(P0)
    du=1/n;
    for k=1:n
        u=k*du;
        [x(:,k+1),y(:,k+1)]=eval_cardinal(P0,P1,P2,P3,T,u);
    end
end

function [x,y] =eval_cardinal(P0,P1,P2,P3,T,u)

    s= (1-T)./2;
    
    % MC is cardinal matrix
    MC=[-s     2-s   s-2        s;
        2.*s   s-3   3-(2.*s)   -s;
        -s     0     s          0;
        0      1     0          0];

    U=[u.^3    u.^2    u    1];

    Gx =[P0(1);P1(1);P2(1);     P3(1)];
    Gy =[P0(2);P1(2);P2(2);     P3(2)];

    xu = (MC'*U')';
    
    x = xu*Gx;    
    y = xu*Gy;
    
    %x = (U*MC*Gx);
    %y = (U*MC*Gy);
    
end