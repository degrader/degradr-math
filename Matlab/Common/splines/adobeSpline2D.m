%%
%

function curve = adobeSpline2D(x,y,xi)    
    curve = zeros(length(xi),1);    
    S = splineSlopes(x,y);
    for i = 1:length(xi)
        curve(i)= splineEvaluate(xi(i),x,y,S);
    end    
    curve(curve>max(y))=max(y);
    curve(curve<min(y))=min(y);
end


function S = splineSlopes(X,Y)

	count = length(X);
	
    start = 1;
    endp  = count;
	
	A =  X (start+1) - X (start);
	B = (Y (start+1) - Y (start)) / A;
	
	S = zeros(count+1);

	S (start) = B;
	
    for j = start + 2:endp		

        C = X (j) - X (j-1);
        D = (Y (j) - Y (j-1)) / C;

        S (j-1) = (B * C + D * A) / (A + C);

        A = C;
        B = D;

    end

	S (endp)   = 2.0 * B - S(endp-1);
	S (start) = 2.0 * S (start) - S (start+1);

	if ((endp - start) > 2)
		
		E = zeros (count+1);
		F = zeros (count+1);
		G = zeros (count+1);

		F (start) = 0.5;
		E (endp)  = 0.5;
		G (start) = 0.75 * (S (start) + S (start+1));
		G (endp)  = 0.75 * (S (endp-1) + S (endp));

		for j = start+1:endp - 1
			

			A = (X (j+1) - X (j-1)) * 2.0;

			E (j) = (X (j+1) - X (j)) / A;
			F (j) = (X (j) - X (j-1)) / A;
			G (j) = 1.5 * S (j);

        end

		for j = start+1:endp
			
			A = 1.0 - F (j-1) * E (j);

			if (j ~= endp) 
                F (j) = F(j) / A;
            end

			G (j) = (G (j) - G (j-1) * E (j)) / A;

        end

		for j=flip(start:endp-1)
			G (j) = G (j) - F (j) * G (j+1);
        end

		for j = start:endp
			S (j) = G (j);
        end
                
    end
    
end

function y = splineEvaluate(x,X,Y,S)
	
    count = length(X);

    % Check for off each end of point list.

    if x <= X(1)
        y = Y(1);
        return;
    end

	if x >= X(count)
		y =  Y(count);
        return;
    end

	% Binary search for the index.
	
	lower = 1;
	upper = count;
	
    while (upper > lower)

        mid = floor((lower + upper) / 2);

        if x == X(mid)		
            y = Y(mid);
            return;
        end

        if x > X(mid)
            lower = mid + 1;
        else
            upper = mid;		        
        end

    end
		
	j = lower;
		
	% X [j - 1] < x <= X [j]
	% A is the distance between the X [j] and X [j - 1]
	% B and C describe the fractional distance to either side. B + C = 1.

	% We compute a cubic spline between the two points with slopes
	% S[j-1] and S[j] at either end. Specifically, we compute the 1-D Bezier
	% with control values:
	%
	%		Y[j-1], Y[j-1] + S[j-1]*A, Y[j]-S[j]*A, Y[j]
	
	y = splineSegment (x,...
                      X (j - 1),...
                      Y (j - 1),...
                      S (j - 1),...
                      X (j    ),...
                      Y (j    ),...
                      S (j    ));
end


function y = splineSegment (x,x0,y0,s0,x1,y1,s1)
	
	A = x1 - x0;

	B = (x - x0)/A;

	C = (x1 - x)/A;

	y = ((y0 * (2.0 - C + B) + (s0 * A * B)) * (C * C)) +...
			   ((y1 * (2.0 - B + C) - (s1 * A * C)) * (B * B));
			   
end
