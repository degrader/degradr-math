%% canonicalSpline2D(X,Y,tension,resolution) - create canonical spline with control points defined for x and y coordinates.
% X-Control coordinates must be normolized to integer. The implementation
% of spline is useful to create interpolated textures that can be used in
% vertex or fragment shaders.
%
%

function [x,y] = canonicalSpline2D(X,Y,tension,resolution)

    Px = [X(1), X(1:end), X(end)]*resolution;
    Py = [Y(1),          Y(1:end), Y(end)];        
    
    x = [];
    y = [];
    for k=1:length(Px)-3
        P0 = [Px(k),  Py(k)];
        P1 = [Px(k+1),Py(k+1)];
        P2 = [Px(k+2),Py(k+2)]; 
        P3 = [Px(k+3),Py(k+3)];
        fprintf(' 2d[%i,%i,%i,%i] %4.3f,%4.3f, %2.3f,%2.3f, %2.3f,%2.3f, %2.3f,%2.3f \n',k, k+1, k+2, k+3, P0, P1, P2, P3)        
        [xx,yy] = canonicalSpline2DSegment(P0,P1,P2,P3,tension); 
        x = [x, xx];
        y = [y, yy]; 
    end
    x = [x/resolution X(end)]; 
    y = [y Y(end)]; y(end) = Y(end); y(1)=Y(1);
end