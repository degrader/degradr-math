%%
%

function y = sigmoid_curve(x,ascent)
    xs = x;
    xx = xs/max(xs);
    %y = sigmoid(xx/(1/2^ascent*2.0))-0.5; 
    y = sigmoid(xx/ascent).*x;
    y=y/(max(y));
end