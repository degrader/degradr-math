%%
%

function y = gamma_exposure(x,g,e)
    m = max(x);
    x=x/m;
    em = e;    
    slope = x * power(2,em);
    
    if e>=0
        gm = egamma_inv_m(slope,g*abs(e));
    else
        gm = egamma_m(slope,g*(abs(e)));
    end

    y = slope;
    
    y(y>1)=1;
    y(y<0)=0;
    
    %y = blend(x,y,abs(e));
end

function y = blend(base, overlay, opacity)
    c2 = base;
    c1 = overlay;
        
    a = opacity + (1.0 - opacity);
    alphaDivisor = a;
    
    if alphaDivisor==0
        alphaDivisor=1.0;
    end
    
    y = (c1 * opacity + c2 * (1.0 - opacity))./alphaDivisor;
    
end


function y = egamma_inv_m(x,gamma)
    y = 1-(1-x).^(gamma);
end


function y = egamma_m(x,gamma)
    y = x.^(gamma);
end
