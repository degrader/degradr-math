%%
%

function y = egamma(x,g)
    m = max(x);
    x=x/m;
    
    gamma = 1.0 / g;	
	slope0 = 32.0;
	x1 = 8.2118790552e-4; 		% pow (slope0, 1.0 / (gamma - 1.0)) * 2.0
	y1 = 0.019310851;			% pow (x1, gamma)
	slope1 = 13.064306598;		% gamma * pow (x1, gamma - 1.0)
    
    y=zeros(1,length(x));
    for i=1:length(x)
        if x(i)<=x1
            y(i) = splineSegment (x(i),0.0,0.0,slope0,x1,y1,slope1);
        else            
            y(i) = x(i)^(gamma);
        end
    end
    y=y;
end

function s = splineSegment (x,x0,y0,s0,x1,y1,s1)
	
	A = x1 - x0;
	B = (x - x0)./A;
	C = (x1 - x)./A;

	s = ((y0.*(2.0 - C + B) + (s0 * A * B)) * (C * C)) +...
        ((y1.*(2.0 - B + C) - (s1 * A * C)) * (B * B));
			   	
end
