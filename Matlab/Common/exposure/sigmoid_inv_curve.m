%%
%

function y = sigmoid_inv_curve(x,a)
    xx = x/max(x);
    ascent = a;
    if ascent<=0
        y = sigmoid_inv((xx)*ascent).*xx; 
    else
        y = (xx-1.)./sigmoid_inv((xx)*-ascent)+1; 
    end
    m = (max(y));
    if m==0
        m=1;
    end
end

function g = sigmoid_inv(z)
    g =1./exp(z);
end
