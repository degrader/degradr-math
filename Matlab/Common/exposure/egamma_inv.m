%%
%

function y = egamma_inv(x,gamma)
    m = max(x);
    x=x/m;
    y = 1-(1-x).^(gamma);
    y=y*m;
end

%%
