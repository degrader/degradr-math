%% g = sigmoid_ascent(z,ascent) - function is a mathematical function having an "S" shape (sigmoid curve) wit ascent.
% Often, sigmoid function refers to the special case of the logistic function shown in the first figure and defined by the formula
% S(t) = 1/(1 + e(-t)).
%

function g = sigmoid_ascent(z,ascent)
    m = sigmoid_ascent_i(max(z),ascent);
    g = (sigmoid_ascent_i(z,ascent)/m-0.5)*2.0;
    %g = 1.0 ./ (1.0 + exp(-z/ascent));
end

function g = sigmoid_ascent_i(z,ascent)
    g = 1.0 ./ (1.0 + exp(-z/ascent));
end
