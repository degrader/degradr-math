%%
%

function [y,tone] = dng_exposure(x,e)
    m = max(x);
    x = x/m;
    
    exposure = abs(e);
        
    [slope,a,b,c] = dng_exposure_props(exposure);
    
    y = zeros(1,length(x));
    
    for i=1:length(x)
        y(i) = dng_exposure_eval(x(i),exposure,slope,a,b,c);
    end               
    tone = y;
    
    if e<0
        y = catmullromSpline2D(y*m,x*m,x*m)/m;
    end
    
end

function y = dng_exposure_eval(x,exposure,slope,a,b,c)
    m = 0.25;
    if x<=m
        y = x * slope;		
    else
        y = x*(x*a + b) + c;
    end
    
    if exposure<0
        y = y*slope;
    end

    if y<0
        y=0;
    elseif y>1
        y=1;
    end
    

end

function x1 = dng_exposure_eval_invers (y,exposure,slope,a,b,c)
	
	kMaxIterations = 30;
	kNearZero      = 1.0e-10;
	
	x0 = 0.0;
	y0 = dng_exposure_eval (x0,exposure,slope,a,b,c);
	
	x1 = 1.0;
	y1 = dng_exposure_eval (x1,exposure,slope,a,b,c);
	
	for iteration=0:kMaxIterations-1	
        
		if abs(y1 - y0) < kNearZero			
            break;
        else
            x2 = pin(0.0, x1 + (y - y1) * (x1 - x0) / (y1 - y0), 1.0);
		
            y2 = dng_exposure_eval (x2,exposure,slope,a,b,c);
		
            x0 = x1;
            y0 = y1;
		
            x1 = x2;
            y1 = y2;		            
        end
					
    end	
end
	
function y = pin (minValue, x, maxValue)		
	y = max(minValue, min(x, maxValue));	
end


function [slope, a, b, c] = dng_exposure_props(e)
    slope = 2^e;   
    a = 16.0 / 9.0 * (1.0 - slope);
    b = (slope - 0.5 * a);
    c = (1.0 - a - b);
end
