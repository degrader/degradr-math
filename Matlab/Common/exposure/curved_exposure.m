%%
%

function y = curved_exposure(x,e)
    m = max(x);
    xi = x;
    mp = mean(x);
    if e>=0
        pp = [
              0.0,   0.0;
              mp/2*pow(2,-e*log2(1+e*2)),    mp/2*pow(2,-e*log2(1+e/64));
              m*pow(2,-e/4),  m
             ];
        %xpoints = [pp(1,1), pp(2,1), pp(3,1), pp(4,1)];
        %ypoints = [pp(1,2), pp(2,2), pp(3,2), pp(4,2)];
        xpoints = [pp(1,1), pp(2,1), pp(3,1)];
        ypoints = [pp(1,2), pp(2,2), pp(3,2)];
    else
        pp = [
              0.0,   0.0;
              mp,      mp*pow(2,e);
              m,       m *pow(2,e/4)
             ];
        xpoints = [pp(1,1), pp(2,1), pp(3,1)];
        ypoints = [pp(1,2), pp(2,2)  pp(3,2)];
    end
    
    %y = catmullromSpline2D(xpoints,ypoints,xi);
    y = adobeSpline2D(xpoints,ypoints,xi);
    y=y/m;
end

function y = pow(x,y)
  y = power(x,y);
end
