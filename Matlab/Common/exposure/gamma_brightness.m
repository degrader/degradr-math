%%
%

function y = gamma_brightness(x,g,b)
    m = max(x);
    x=(x-m/g*(1-b))/m;
%    y = sigmoid_ascent_m(x-10,3);
%     em = b;
%     gm = g;
%     y = egamma_inv_m(x,gm);
    y = x * b;
    y(y>1)=1;
    y(y<0)=0;
%     %y = egamma(y,1/gm);
%      ym = sigmoid_ascent_m(0-10,3);% min(y);
%      y = y-ym;
%      y = y/sigmoid_ascent_m(10-10,3); %max(y);
%      y = y * b;
%      y(y>1)=1;
%      y(y<0)=0;
     y = y*m;
end

function y = egamma_inv_m(x,gamma)
    y = 1-(1-x).^(gamma);
end

function g = sigmoid_ascent_m(z,ascent)
    g = 1.0 ./ (1.0 + exp(-z/ascent));
end
