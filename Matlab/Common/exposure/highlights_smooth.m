function y = highlights_smooth(x,weight,width,ascent)
    maxe = highlights_smooth_internal(1,weight,width,ascent);
    y =  weight * highlights_smooth_internal(x,weight,width,ascent)./maxe;
end

function z = highlights_smooth_internal(x,weight,width,ascent)
    w = width * 0.3;
    a = ascent * 10.0;
    z = zeros(length(x));
    for i=1:length(x)
        y = ((1.0-x(i)) * a / w )^2;
        %y = (1.0-x(i)) * a / (w);
        z(i) = weight/exp(y) * (w);
    end
end