%%
%
function y = exposure(x,g,a,e)
    m = max(x);
    x = x/m;
    x = egamma(x,1/g);
    y = x * power(2,e)+e/m*a;  
    y = egamma(y,g);
    y(y>1)=1;
    y(y<0)=0;
    y=y*m;
end
