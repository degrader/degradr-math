%%
%


function y = dng_exposure_ramp(x,white,black,minBlack)

    m=max(x);
    x=x/m;
    [slope,fblack,radius,scale] = dng_exposure_ramp_props(white,black,minBlack);

    y=zeros(1,length(x));
    
    for i=1:length(x)
        if x(i)<=(fblack - radius)
            y(i)=0.0;
        elseif (x(i) >= fblack + radius)
            y(i)=min((x(i) - fblack) * slope, 1.0);
        else
            y(i) = x(i) - (fblack - radius);
            y(i) = y(i)^2*scale ;
        end
    end
end

function [slope,fblack,radius,scale] = dng_exposure_ramp_props(white,black,minBlack)
    slope = 1.0 / (white - black);
    maxCurveX = 0.5;	
	maxCurveY = 1.0 / 16.0;	
	fblack = black;
	radius = min (maxCurveX * minBlack, maxCurveY/slope);
	
	if (radius > 0.0)
		scale= slope / (4.0 * radius);
	else
		scale = 0.0;
    end
end
    